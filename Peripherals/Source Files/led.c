#include "led.h"

//LED 0-7
const port_t PORTS_LEDS [] = {{0,0},
															{0,2},
															{0,22},
															{2,6},
															{2,7},
															{2,8},
															{2,12},
															{2,13}};

void LED_Init(void)
{
	int i;
	GPIO_Init();
	for (i = 0; i < sizeof(PORTS_LEDS) / sizeof(port_t); i++)
	{
		GPIOSetPinSel	(PORTS_LEDS[i], PINSEL_GPIO);
		GPIOSetDir		(PORTS_LEDS[i], DIRECTION_OUTPUT);
		GPIOSetValue 	(PORTS_LEDS[i], 0);
	}
}

void LED_On(unsigned int num)
{
	GPIOSetValue 	(PORTS_LEDS[num], 1);
}
	
void LED_Off (unsigned int num)
{
	GPIOSetValue 	(PORTS_LEDS[num], 0);
}
	
void LED_Out (unsigned char value)	
{	
	int i;
	for (i = 0; i < 8; i++)
	{
		if (value & (1UL << i)) 
			LED_On(i);
		else 
			LED_Off(i);
	}
}	

int LED_Value (unsigned char num)
{
	return GPIOGetValue(PORTS_LEDS[num]);
}
	
void LED_Toggle(unsigned int num)
{
	if (LED_Value(num) == 1UL)
		LED_Off(num);
	else
		LED_On(num);
}
