#include "rgb.h"

//Leds in der richtigen Reihenfolge
//P0.10 Blau
//P0.11 Rot
//P4.29 Gruen
const port_t PORTS_RGB[] = {{0,10}, 
														{0,11}, 
														{4,29}};

void RGB_Init(void)
{
	int i;
	GPIO_Init();
	for (i = 0; i < sizeof(PORTS_RGB) / sizeof(port_t); i++)
	{
		GPIOSetPinSel	(PORTS_RGB[i], PINSEL_GPIO);
		GPIOSetDir		(PORTS_RGB[i], DIRECTION_OUTPUT);
		GPIOSetValue	(PORTS_RGB[i], 0);
	}
}

void RGB_On(uint8_t num)
{
	GPIOSetValue(PORTS_RGB[num], 1);
}

void RGB_Off(uint8_t num)
{
	GPIOSetValue(PORTS_RGB[num], 0);
}

void RGB_Out (uint8_t value)
{
	int i;
	for (i = 0; i < 3; i++)
	{
		if (value & (1 << i))
			RGB_On(i);
		else
			RGB_Off(i);
	}
}
