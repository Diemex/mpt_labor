/*==============================================================================
SOURCE FILE gpio.c:
--------------------------------------------------------------------------------
Datum: 
--------------------------------------------------------------------------------
Autor: 
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Beschreibung: Definitionen/ Implementierungen v. Bibliotheksfunktionen
           -> GPIOSetDir()
           -> GPIOSetValue()
					 -> GPIOGetValue()

--------------------------------------------------------------------------------
Hinweise:     Funktionsprotoytpen/ Deklarationen v. Bibliotheksfunktionen
           -> siehe Header File gpio.h
================================================================================
*/

//==============================================================================
//Einbinden v. Header Files:
//==============================================================================
#include "gpio.h"


//==============================================================================
//Definition v. globalen Variablen:
//==============================================================================


//==============================================================================
//Definitionen/ Implementierungen v. Bibliotheksfunktionen:
//==============================================================================

//
/*****************************************************************************
 ** Function name:               GPIOSetDir()
 **
 ** Descriptions:                Funktion zum Einstellen der Portrichtung 
 **															 eines Port-Pins:
 ** Parameters:                  port num: Nummer des Ports, 
 **										 					 bit position: Nummer des Port-Pins, 
 **															 dir: Richtung (1 out, 0 input)
 ** Returned value:              None
 **
 *****************************************************************************/

void GPIOSetDir_old(uint8_t portNum, uint8_t bitPosi, uint8_t dir) 
{
  switch ( portNum )
  {
	case PORT0:
	  if ( !(LPC_GPIO0->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_OUTPUT) )
			LPC_GPIO0->FIODIR |= (0x1<<bitPosi);
	  else if ( (LPC_GPIO0->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_INPUT) )
			LPC_GPIO0->FIODIR &= ~(0x1<<bitPosi);
	break;
 	case PORT1:
		if ( !(LPC_GPIO1->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_OUTPUT) )
			LPC_GPIO1->FIODIR |= (0x1<<bitPosi);
	  else if ( (LPC_GPIO1->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_INPUT) )
			LPC_GPIO1->FIODIR &= ~(0x1<<bitPosi);
	break;
	case PORT2:
		if ( !(LPC_GPIO2->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_OUTPUT) )
			LPC_GPIO2->FIODIR |= (0x1<<bitPosi);
	  else if ( (LPC_GPIO2->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_INPUT) )
			LPC_GPIO2->FIODIR &= ~(0x1<<bitPosi);
	break;
	case PORT3:
		if ( !(LPC_GPIO3->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_OUTPUT) )
			LPC_GPIO3->FIODIR |= (0x1<<bitPosi);
	  else if ( (LPC_GPIO3->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_INPUT) )
			LPC_GPIO3->FIODIR &= ~(0x1<<bitPosi);
	break;
	case PORT4:
		if ( !(LPC_GPIO4->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_OUTPUT) )
			LPC_GPIO4->FIODIR |= (0x1<<bitPosi);
	  else if ( (LPC_GPIO4->FIODIR & (0x1<<bitPosi)) && (dir == GPIO_INPUT) )
			LPC_GPIO4->FIODIR &= ~(0x1<<bitPosi);
	break;
	
	default: break;
  }
  return;

}

//
/*****************************************************************************
 ** Function name:               GPIOSetValue()
 **
 ** Descriptions:                Funktion zum Ausgeben eines Wertes an 
 **															 einem Port-Pin (Setzen/Rücksetzen):
 ** Parameters:                  port num: Nummer des Ports, 
 **										 					 bit position: Nummer des Port-Pins, 
 **															 bitVal: Wert (1 , 0 )
 ** Returned value:              None
 **
 *****************************************************************************/

void GPIOSetValue_old(uint8_t portNum, uint8_t bitPosi, uint8_t bitVal) 
{
	if (bitVal == 1 ) 
		{ 
			/* Port-Pin auf 1 setzen  		*/
			switch ( portNum ) 
			{ case PORT0: 
					LPC_GPIO0->FIOSET = (1<<bitPosi); 
				break; 
				case PORT1: 
					LPC_GPIO1->FIOSET = (1<<bitPosi); 
				case PORT2: 
					LPC_GPIO2->FIOSET = (1<<bitPosi); 
				case PORT3: 
					LPC_GPIO3->FIOSET = (1<<bitPosi); 
				case PORT4: 
					LPC_GPIO4->FIOSET = (1<<bitPosi); 
				default: break; 
			}
		}
	else if (bitVal == 0 ) 
		{ 
			/* Port-Pin auf 0 setzen  		*/
			switch ( portNum ) 
			{ case PORT0: 
					LPC_GPIO0->FIOCLR = (1<<bitPosi); 
				break; 
				case PORT1: 
					LPC_GPIO1->FIOCLR = (1<<bitPosi); 
				case PORT2: 
					LPC_GPIO2->FIOCLR = (1<<bitPosi); 
				case PORT3: 
					LPC_GPIO3->FIOCLR = (1<<bitPosi); 
				case PORT4: 
					LPC_GPIO4->FIOCLR = (1<<bitPosi); 
				default: break; 
			}
		}
		
  return;
}


//
/*****************************************************************************
 ** Function name:               GPIOGetValue()
 **
 ** Descriptions:                Funktion zum Einlesen eines Wertes an 
 **															 einem Port-Pin :
 ** Parameters:                  port num: Nummer des Ports, 
 **										 					 bit position: Nummer des Port-Pins, 
 **															 
 ** Returned value:              None
 **
 *****************************************************************************/

uint8_t GPIOGetValue_old(uint8_t portNum, uint8_t bitPosi) 
{			
	uint32_t value=0;
	switch ( portNum ) 
	{ case PORT0: 
			value = ((LPC_GPIO0->FIOPIN) & (1<<bitPosi)); 
		break; 
		case PORT1: 
			value = ((LPC_GPIO1->FIOPIN) & (1<<bitPosi)); 	 
		case PORT2: 
			value = ((LPC_GPIO2->FIOPIN) & (1<<bitPosi)); 	 
		case PORT3: 
			value = ((LPC_GPIO3->FIOPIN) & (1<<bitPosi)); 	 
		case PORT4: 
			value = ((LPC_GPIO4->FIOPIN) & (1<<bitPosi));  
		default: break; 
	}
		if(value)
			value = 1;		
  return value;			
}

/***
 *      _____ _                         _____            _    _   _                        
 *     | ____(_) __ _  ___ _ __   ___  |  ___|   _ _ __ | | _| |_(_) ___  _ __   ___ _ __  
 *     |  _| | |/ _` |/ _ \ '_ \ / _ \ | |_ | | | | '_ \| |/ / __| |/ _ \| '_ \ / _ \ '_ \ 
 *     | |___| | (_| |  __/ | | |  __/ |  _|| |_| | | | |   <| |_| | (_) | | | |  __/ | | |
 *     |_____|_|\__, |\___|_| |_|\___| |_|   \__,_|_| |_|_|\_\\__|_|\___/|_| |_|\___|_| |_|
 *              |___/                                                                      
 */

void GPIO_Init(void)
{
	LPC_SC->PCONP |= (1 << 15);
}

uint8_t getX2(port_t portPin){
	return portPin.port * 2 + portPin.pin / 16;
}

void GPIOSetPinSel(port_t portPin, pinselmode_t mode/*2 bits*/)
{
	uint8_t x = getX2(portPin);
	int shift = portPin.pin % 16;
	switch(x)
	{
		case 0:
			LPC_PINCON->PINSEL0 &= mode << shift;
			LPC_PINCON->PINSEL0 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 1:
			LPC_PINCON->PINSEL1 &= mode << shift;
			LPC_PINCON->PINSEL1 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 2:
			LPC_PINCON->PINSEL2 &= mode << shift;
			LPC_PINCON->PINSEL2 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 3:
			LPC_PINCON->PINSEL3 &= mode << shift;
			LPC_PINCON->PINSEL3 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 4:
			LPC_PINCON->PINSEL4 &= mode << shift;
			LPC_PINCON->PINSEL4 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 5:
			LPC_PINCON->PINSEL5 &= mode << shift;
			LPC_PINCON->PINSEL5 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 6:
			LPC_PINCON->PINSEL6 &= mode << shift;
			LPC_PINCON->PINSEL6 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 7:
			LPC_PINCON->PINSEL7 &= mode << shift;
			LPC_PINCON->PINSEL7 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 8:
			LPC_PINCON->PINSEL8 &= mode << shift;
			LPC_PINCON->PINSEL8 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 9:
			LPC_PINCON->PINSEL9 &= mode << shift;
			LPC_PINCON->PINSEL9 &= ( ~(0xF << shift)) | (mode << shift);
			break;
	}
}


void GPIOSetPinMode(port_t portPin, pinmode_t mode/*2 bits*/)
{
	uint8_t x = getX2(portPin);
	uint8_t shift = portPin.pin % 16;
	switch(x)
	{
		case 0:
			//Positive bits setzen
			LPC_PINCON->PINMODE0 |= (mode << shift);
			//Negative bits (4 -> F) setzen
			LPC_PINCON->PINMODE0 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 1:
			LPC_PINCON->PINMODE1 |= (mode << shift);
			LPC_PINCON->PINMODE1 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 2:
			LPC_PINCON->PINMODE2 |= (mode << shift);
			LPC_PINCON->PINMODE2 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 3:
			LPC_PINCON->PINMODE3 |= (mode << shift);
			LPC_PINCON->PINMODE3 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 4:
			LPC_PINCON->PINMODE4 |= (mode << shift);
			LPC_PINCON->PINMODE4 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 5:
			LPC_PINCON->PINMODE5 |= (mode << shift);
			LPC_PINCON->PINMODE5 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 6:
			LPC_PINCON->PINMODE6 |= (mode << shift);
			LPC_PINCON->PINMODE6 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 7:
			LPC_PINCON->PINMODE7 |= (mode << shift);
			LPC_PINCON->PINMODE7 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 8:
			LPC_PINCON->PINMODE8 |= (mode << shift);
			LPC_PINCON->PINMODE8 &= ( ~(0xF << shift)) | (mode << shift);
			break;
		case 9:
			LPC_PINCON->PINMODE9 |= (mode << shift);
			LPC_PINCON->PINMODE9 &= ( ~(0xF << shift)) | (mode << shift);
			break;
	}
}

extern void GPIOSetDir(port_t port, direction_t dir)
{
	GPIOSetDir_old(port.port, port.pin, dir);
}

extern void GPIOSetValue(port_t port, uint8_t bitVal)
{
	GPIOSetValue_old(port.port, port.pin, bitVal);
}

extern uint8_t GPIOGetValue(port_t port)
{
	return GPIOGetValue_old(port.port, port.pin);
}
