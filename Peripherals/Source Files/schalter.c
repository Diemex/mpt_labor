#include "schalter.h"

//S0-S7
const port_t PORTS_SCHALTER [] = {{1,18},
																	{1,19},
																	{1,20},
																	{1,21},
																	{1,22},
																	{1,23},
																	{1,24},
																	{1,25}};

void SCHALTER_Init(void)
{
	int i;
	GPIO_Init();
	for (i = 0; i < sizeof(PORTS_SCHALTER) / sizeof(port_t); i++)
	{
		GPIOSetPinSel	(PORTS_SCHALTER[i], PINSEL_GPIO);
		GPIOSetDir		(PORTS_SCHALTER[i], DIRECTION_INPUT);
		GPIOSetPinMode(PORTS_SCHALTER[i], PIN_MODE_NEITHER_PULL_UP_NOR_PULL_DOWN);
	}	
}

uint8_t SCHALTER_GetPositionen(void)
{
	return (uint8_t) (LPC_GPIO1->FIOPIN >> 18);
}
