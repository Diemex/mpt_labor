#include "taster.h"

//TA10, TA11, TA11
const port_t PORTS_TASTER[] = {{1, 26},
															 {1, 27},
															 {2, 11}};

void TASTER_Init(void)
{
	int i;
	GPIO_Init();
	for (i = 0; i < sizeof(PORTS_TASTER) / sizeof(port_t); i++)
	{
		GPIOSetPinSel	(PORTS_TASTER[i], PINSEL_GPIO);
		GPIOSetDir		(PORTS_TASTER[i], DIRECTION_INPUT);
		GPIOSetPinMode(PORTS_TASTER[i], PIN_MODE_NEITHER_PULL_UP_NOR_PULL_DOWN);
	}
}

uint8_t TASTER_Get_TA10(void)
{
	return GPIOGetValue(PORTS_TASTER[0]);
}

uint8_t TASTER_Get_TA11(void)
{
	return GPIOGetValue(PORTS_TASTER[1]);
}

uint8_t TASTER_Get_TA12(void)
{
	return GPIOGetValue(PORTS_TASTER[2]);
}

uint8_t TASTER_GetAll(void)
{
	//TA11 : 2, TA12 : 1, TA10 : 0 - Schalterriehenfolge so das es f�r Aufgabe 1.9 passt
	//Inline ifs beeeeeschde programmier stil, aber ist ja c also ist programmierstil scheisseeeeeeeeegal
	// Inline if = ifBedingung ? thenDasHier : elseDasHier
	return (uint8_t) ((TASTER_Get_TA11() ? (1 << 2) : 0) |
										(TASTER_Get_TA12() ? (1 << 1) : 0) |
										(TASTER_Get_TA10() ? (1 << 0) : 0));
}
