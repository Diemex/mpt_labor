#include "joystick.h"

//UP:				0.21	
//DOWN:			0.27
//LEFT:			0.25
//RIGHT:		0.28
//CENTER:		0.3

const uint8_t UP 			= 0;
const uint8_t DOWN 		= 1;
const uint8_t LEFT 		= 2;
const uint8_t RIGHT 	= 3;
const uint8_t CENTER 	= 4;

const port_t PORTS_JOYSTICK [] = {{0,21},
																	{0,27}, 
																	{0,25}, 
																	{0,28}, 
																	{0,3}};

extern void JOYSTICK_Init(void)
{
	int i;
	GPIO_Init();
	for (i = 0; i < sizeof(PORTS_JOYSTICK) / sizeof(port_t); i++)
	{
		GPIOSetPinSel	(PORTS_JOYSTICK[i], PINSEL_GPIO);
		GPIOSetDir		(PORTS_JOYSTICK[i], DIRECTION_INPUT);
		GPIOSetPinMode(PORTS_JOYSTICK[i], PIN_MODE_NEITHER_PULL_UP_NOR_PULL_DOWN);
	}
}

extern uint8_t JOYSTICK_GetLeft(void)
{
	return GPIOGetValue(PORTS_JOYSTICK[LEFT]);
}

extern uint8_t JOYSTICK_GetRight(void)
{
	return GPIOGetValue(PORTS_JOYSTICK[RIGHT]);
}

extern uint8_t JOYSTICK_GetUp(void)
{
	return GPIOGetValue(PORTS_JOYSTICK[UP]);
}

extern uint8_t JOYSTICK_GetDown(void)
{
	return GPIOGetValue(PORTS_JOYSTICK[DOWN]);
}

extern uint8_t JOYSTICK_GetCenter(void)
{
	return GPIOGetValue(PORTS_JOYSTICK[CENTER]);
}
