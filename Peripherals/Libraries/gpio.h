/*================================================================================
HEADER FILE gpio.h:
--------------------------------------------------------------------------------
Datum: 
--------------------------------------------------------------------------------
Autor: 
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Beschreibung: Funktionsprotoytpen/ Deklarationen v. Bibliotheksfunktionen
           -> GPIOSetDir()
           -> GPIOSetValue()
	   -> GPIOGetValue()

--------------------------------------------------------------------------------
Hinweise:     Definitionen/ Implementierungen v. Bibliotheksfunktionen
           -> siehe Source File gpio.c
================================================================================
*/

#ifndef __GPIO_H //Anfang bedingte Kompilierung
#define __GPIO_H

#include <LPC17xx.h>

//================================================================================
//Definition v. Konstanten:
//================================================================================
#define PORT0 0
#define PORT1 1
#define PORT2 2
#define PORT3 3
#define PORT4 4

#define PORT_PIN2 2

#define PORT_PIN_HIGH 1
#define PORT_PIN_LOW 0
#define GPIO_OUTPUT 1
#define GPIO_INPUT 0



//================================================================================
//Funktionsprototypen/ Deklarationen v. Bibliotheksfunktionen:
//================================================================================

//--------------------------------------------------------------------------------
//Funktion zum Einstellen der Portrichtung (dir=1: Output) eines Port-Pins:
//--------------------------------------------------------------------------------
extern void GPIOSetDir_old(uint8_t portNum, uint8_t bitPosi, uint8_t dir);

//--------------------------------------------------------------------------------
//Funktion zum Ausgeben eines Wertes an einem Port-Pin (Setzen/Rücksetzen):
//--------------------------------------------------------------------------------
extern void GPIOSetValue_old (uint8_t portNum, uint8_t bitPosi, uint8_t bitVal);

//--------------------------------------------------------------------------------
//Funktion zum Einlesen eines Wertes an einem Port-Pin:
//--------------------------------------------------------------------------------
uint8_t GPIOGetValue_old(uint8_t portNum, uint8_t bitPosi) ;

/***
 *      _____ _                         _____            _    _   _                        
 *     | ____(_) __ _  ___ _ __   ___  |  ___|   _ _ __ | | _| |_(_) ___  _ __   ___ _ __  
 *     |  _| | |/ _` |/ _ \ '_ \ / _ \ | |_ | | | | '_ \| |/ / __| |/ _ \| '_ \ / _ \ '_ \ 
 *     | |___| | (_| |  __/ | | |  __/ |  _|| |_| | | | |   <| |_| | (_) | | | |  __/ | | |
 *     |_____|_|\__, |\___|_| |_|\___| |_|   \__,_|_| |_|_|\_\\__|_|\___/|_| |_|\___|_| |_|
 *              |___/                                                                      
 */

typedef struct port_struct 
{
	uint8_t port; 
	uint8_t pin;
} port_t;

typedef enum pinmode
{
	PIN_MODE_PULL_UP 												= 0,
	PIN_MODE_REPEATER 											= 1,
	PIN_MODE_PULL_DOWN 											= 2,
	PIN_MODE_NEITHER_PULL_UP_NOR_PULL_DOWN 	= 3
} pinmode_t;

typedef enum pinselmode
{
	PINSEL_GPIO 	= 0,
	PINSEL_MODE1 	= 1,
	PINSEL_MODE2 	= 2,
	PINSEL_MODE3	= 3
}pinselmode_t;

typedef enum direction
{
	DIRECTION_INPUT  = GPIO_INPUT,
	DIRECTION_OUTPUT = GPIO_OUTPUT
} direction_t;
 
extern void GPIO_Init(void);

extern uint8_t getX2(port_t port);

extern void GPIOSetPinMode(port_t port , pinmode_t mode/*2 bits*/);

extern void GPIOSetPinSel(port_t port, pinselmode_t mode/*2 bits*/);

extern void GPIOSetDir(port_t port, direction_t dir);

extern void GPIOSetValue(port_t port, uint8_t bitVal);

extern uint8_t GPIOGetValue(port_t port);

#endif //Ende bedingte Kompilierung
