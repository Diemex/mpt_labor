#ifndef __LED_H //Anfang bedingte Kompilierung
#define __LED_H

#include <LPC17xx.h>
#include "gpio.h"

extern void LED_Init(void);
extern void LED_On(unsigned int num);
extern void LED_Off (unsigned int num);
extern void LED_Out (unsigned char value);
extern void LED_Toggle(unsigned int num);

#endif //Ende bedingte Kompilierung
