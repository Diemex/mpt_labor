#ifndef __RGB_H //Anfang bedingte Kompilierung
#define __RGB_H

#include <LPC17xx.h>
#include "gpio.h"

#define RGB_BLUE 	0
#define RGB_RED 	1
#define RGB_GREEN 2

extern void RGB_Init(void);
extern void RGB_On(uint8_t num);
extern void RGB_Off(uint8_t num);
extern void RGB_Out (uint8_t value);

#endif //Ende bedingte Kompilierung
