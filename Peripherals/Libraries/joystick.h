#ifndef __JOYSTICK_H //Anfang bedingte Kompilierung
#define __JOYSTICK_H

#include <LPC17xx.h>
#include "gpio.h"

extern void JOYSTICK_Init(void);
extern uint8_t JOYSTICK_GetLeft(void);
extern uint8_t JOYSTICK_GetRight(void);
extern uint8_t JOYSTICK_GetUp(void);
extern uint8_t JOYSTICK_GetDown(void);
extern uint8_t JOYSTICK_GetCenter(void);

#endif //Ende bedingte Kompilierung
