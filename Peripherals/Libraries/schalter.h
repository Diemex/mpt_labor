#ifndef __SCHALTER_H //Anfang bedingte Kompilierung
#define __SCHALTER_H

#include <LPC17xx.h>
#include "gpio.h"

extern void SCHALTER_Init(void);
extern uint8_t SCHALTER_GetPositionen(void);

#endif //Ende bedingte Kompilierung
