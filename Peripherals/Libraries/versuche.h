/*================================================================================
HEADER FILE VERSUCH1.h:
--------------------------------------------------------------------------------
Datum: 21.01.2014
--------------------------------------------------------------------------------
Autor: Bernhard Beetz
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Beschreibung: Steuerung der Compilierung von Versuch1.c
--------------------------------------------------------------------------------

================================================================================
*/

#ifndef __VERSUCHE_H //Anfang bedingte Kompilierung
#define __VERSUCHE_H

//================================================================================
//Einbinden v. Header Files:
//================================================================================
#include <LPC17xx.h>

//================================================================================
//Definition v. Konstanten:
//================================================================================

//********************************************************************************
// Versuch 1
#define V1_1 0
#define V1_2 0
#define V1_3 0
#define V1_4 0
#define V1_5 0
#define V1_6 1
#define V1_7 0
//********************************************************************************

// Versuch 2
#define V2_1 1
#define V2_2 0
#define V2_3 0
#define V2_4 0
#define V2_6 0
#define V2_7 0
//********************************************************************************

// Versuch 3
#define V3_1 1
#define V3_2 0
#define V3_3 0
#define V3_4 0
#define V3_5 0
#define V3_6 0
#define V3_7 0
//********************************************************************************

// Versuch 4
#define V41_1 1
#define V41_2 0
#define V41_3 0
#define V41_4 0
#define V41_5 0
#define V41_6 0
#define V42_1 0
#define V42_2 0
//********************************************************************************

// Versuch 5
#define V5_1 1
#define V5_2 0
#define V5_3 0
#define V5_4 0
#define V5_5 0
#define V5_6 0
#define V5_7 0
//********************************************************************************

// Versuch 6
#define V6_1 1
#define V6_2 0
#define V6_3 0
#define V6_4 0
#define V6_5 0
#define V6_6 0
#define V6_7 0
//********************************************************************************

// Versuch 7
#define V7_1 1
#define V7_2 0
#define V7_3 0
#define V7_4 0

#endif //Ende bedingte Kompilierung
