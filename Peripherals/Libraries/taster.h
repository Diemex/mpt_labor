#ifndef __TASTER_H //Anfang bedingte Kompilierung
#define __TASTER_H

#include <LPC17xx.h>
#include "gpio.h"

extern void TASTER_Init(void);
extern uint8_t TASTER_GetTA10Stat(void);
extern uint8_t TASTER_GetTA11Stat(void);
extern uint8_t TASTER_GetTA12Stat(void);
extern uint8_t TASTER_GetAll(void);

#endif //Ende bedingte Kompilierung
