/******************************************************************************

*       * ***** ***   ***  *   *  *** *   *      *
 *     *  *     *  * *     *   * *    *   *    * *
  *   *   ***** ***    *   *   * *    *****      *
   * *    *     * *      * *   * *    *   *      *
    *     ***** *  *  **** *****  *** *   *      *

-	Zeitverzögerung
- GPIO
-	LEDs
-	Schalter
-	Taster
-	Joystick
-	RGB-LED

	Autor: 
	Datum: 
	       

********************************************************************************/

//================================================================================
//Einbinden v. Header Files:
//================================================================================
#include <LPC17xx.h>
#include "versuche.h"

#include "GLCD.h"
#include "gpio.h"
#include "delay.h"

#include "led.h"
#include "rgb.h"
#include "joystick.h"
#include "schalter.h"
#include "taster.h"


//================================================================================
//Definition v. Konstanten:
//================================================================================


//================================================================================
//  Main-Funktion Versuchsteil V1_1
//  Verzögerungsfunktion delay1ms() und Portausgabe an P2.2 = PWM1.3
//================================================================================
#if (V1_1 == 1)

#define OUT_PORT 0
#define PORT_PIN 0

int main(void)
{	
#ifdef __USE_LCD
  GLCD_Init();                   // Initialize graphical LCD   
  GLCD_Clear(Black);             // Clear graphical LCD display  
#endif	
	GPIOSetDir_old(OUT_PORT, PORT_PIN, GPIO_OUTPUT);
	GPIOSetValue_old(OUT_PORT,PORT_PIN,PORT_PIN_LOW);
	while(1)
	{	GPIOSetValue_old(OUT_PORT,PORT_PIN,PORT_PIN_HIGH);
		delayXms(100);
		GPIOSetValue_old(OUT_PORT,PORT_PIN,PORT_PIN_LOW);	
		delayXms(100);
	} // end while(1)
}	// end main()

#endif

//================================================================================
//  Main-Funktion Versuchsteil V1_2
//  LED0 blinkt mit 1s, LED4 blinkt mit speziellem Rhythmus
//================================================================================
#if (V1_2 == 1)

int main(void)
{	

	while(1)
	{
		
	} // end while(1)
}	// end main()

#endif



//================================================================================
//  Main-Funktion Versuchsteil V1_3
//  Schalterstellung einlesen und auf LEDs ausgeben
//================================================================================
#if (V1_3 == 1)

int main(void)
{	
	LED_Init();
	SCHALTER_Init();	
	while(1)
	{
		LED_Out(SCHALTER_GetPositionen());
	} // end while(1)
}	// end main()

#endif



//================================================================================
//  Main-Funktion Versuchsteil V1_4
//  RGB-LED ansteuern
//================================================================================
#if (V1_4 == 1)

int main(void)
{	
	int i;
	uint8_t flanken, alteStellung = 0UL, neueStellung;
	TASTER_Init();
	RGB_Init();
	while(1)
	{
		neueStellung = TASTER_GetAll();
		flanken = alteStellung & (~neueStellung); 
		for (i = 0; i < 3; i++)
		{
			if (flanken & (1 << i))
				RGB_On(i);
			else 
				RGB_Off(i);
		}
		//aktuelle schalterpos speichern damit wir Flanken registrieren koennen
		alteStellung = neueStellung;
	} // end while(1)
}	// end main()

#endif

//================================================================================
//  Main-Funktion Versuchsteil V1_5
//  Joystick einlesen und LEDs ansteuern
//================================================================================
#if (V1_5 == 1)

int main(void)
{	
	uint8_t farbe = 0UL;
	uint8_t alterWert = JOYSTICK_GetCenter();
	JOYSTICK_Init();
	LED_Init();
	RGB_Init();
	while(1)
	{
		if (JOYSTICK_GetLeft()) LED_Out(1 << 6);
		else if (JOYSTICK_GetRight()) LED_Out(1 << 2);
		else if (JOYSTICK_GetUp()) LED_Out(1 << 0);
		else if (JOYSTICK_GetDown()) LED_Out(1 << 4);
		else LED_Out(0);
		//Pos. Flanke
		if (alterWert != JOYSTICK_GetCenter() && JOYSTICK_GetCenter() == 1)
			RGB_Out(++farbe);
		
	} // end while(1)
}	// end main()

#endif


//================================================================================
//  Main-Funktion Versuchsteil V1_6
//  LED-Lauflicht mit Joystick und Schaltern
//================================================================================
#if (V1_6 == 1)

int main(void)
{	
	uint16_t pos, muster, dir, v = 250, vmax = 50, vmin= 500, vdelta = 50, CLOCKWISE = 0UL, COUNTER_CLOCKWISE = 1UL;	
	JOYSTICK_Init();
	RGB_Init();
	LED_Init();
	SCHALTER_Init();
	while(1)
	{
		muster = SCHALTER_GetPositionen();
		
		if (JOYSTICK_GetLeft() == 1UL) 										
			dir = CLOCKWISE;
		else if (JOYSTICK_GetRight() == 1UL) 							
			dir = COUNTER_CLOCKWISE;
		else if (JOYSTICK_GetUp() == 1UL && v > vmin) 		
			v -= vdelta;
		else if (JOYSTICK_GetDown() == 1UL && v < vmax) 	
			v +=  vdelta;
		
		if (v == vmax) 
			RGB_Out(1 << RGB_RED);
		else if (v == vmin) 
			RGB_Out(1 << RGB_GREEN);
		else 								
			RGB_Out(1 << RGB_BLUE);
		
		//Bits um pos nach links schieben und rechs wieder reinschieben
		RGB_Out(muster << pos | muster >> (8 - pos));
		
		if (dir == CLOCKWISE)
			pos++;
		else
			pos--;
		
		pos %= 8;
		delayXms(v / 50);
	} // end while(1)
}	// end main()

#endif


//================================================================================
//  Main-Funktion Versuchsteil V1_7
//  Taster TA12, TA10 und TA11 einlesen und RGB ansteuern
//================================================================================
#if (V1_7 == 1)

int main(void)
{	

	while(1)
	{
		
	} // end while(1)
}	// end main()

#endif

